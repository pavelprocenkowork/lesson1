import UIKit

let numberValue = "4.3"
let boolValue: Bool = false
let floatValue: Float = 5.3333333333
let doubleValue = 4.3333333333

var str = "str"

if str == "str" && doubleValue > 5 {
    print("yes")
} else if str == "kak" {
    print("no")
} else {
    print(".")
}

switch str {
case "str":
    print("yes")
case "kak":
    print("no")
default:
    print(".")
}

let firstValue: Double = 15.7
let secondValue: Double = 10.5

let resultSummValue = Int(firstValue + secondValue)
print(firstValue + secondValue)

let resultStringValue: String = "it's string + \(firstValue)"

print("-------------------------")

for i in 1...100 {
    print(i)
}

print("-------------------------")

for i in 0...10 where i % 2 == 0 {
    
    print(i)
}

print("-------------------------")
var i = 10
while i > 0 {
    print(i)
    i-=1
}

print("-------------------------")
var k = 10
repeat {
    k-=1
    if k == 5 {
        continue
    }
    
    
    print(k)
    
   
} while k > 0

print("-------------------------")

let numberType = 5.0

if numberType is String {
    print("yes")
} else if numberType is Int {
    print("no")
}

let qwe = Int(numberType)
print(qwe)

